//
//  CalculatorViewController.swift
//  ios-mvcvm
//
//  Created by Stanislaw Brzezinski on 28/01/2017.
//  Copyright © 2017 Stanislaw Brzezinski. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController,ViewUpdatable {
    @IBOutlet weak var sideField: UITextField!
    @IBOutlet weak var areaField: UITextField!
    @IBOutlet weak var diameterLabel: UILabel!
   
    var controller:CalculatorController?
    var vieModel:CalculatorViewModel?
    
    @IBAction func sideFieldOnTextChanged(_ sender: UITextField) {
        controller?.onSideFieldChanged(value: sender.text!)
    }
    
    @IBAction func areaFieldOnTextChanged(_ sender: UITextField) {
        controller?.onAreaFieldChanged(value: sender.text!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpMVCVM();
    }

    func setUpMVCVM(){
        let model=CalculatorModel()
        self.vieModel=CalculatorViewModel(model:model)
        self.controller=CalculatorController(viewUpdatable: self, model: model, viewModel: self.vieModel!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateView() {
        self.areaField.text=vieModel?.getAreaText()
        self.diameterLabel.text=vieModel?.getDiameterText()
        self.sideField.text=vieModel?.getSideText()
    }

}
