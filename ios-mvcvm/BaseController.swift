//
//  BaseController.swift
//  ios-mvcvm
//
//  Created by Stanislaw Brzezinski on 28/01/2017.
//  Copyright © 2017 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
class BaseController<M, VM:BaseViewModel<M>>{
    var model:M
    var viewModel:VM
    var viewUpdatable:ViewUpdatable
    
    required init (viewUpdatable:ViewUpdatable,model:M,viewModel:VM){
        self.model=model
        self.viewModel=viewModel
        self.viewUpdatable=viewUpdatable
    }
    
    func updateView(){
        viewUpdatable.updateView()
    }
    
    
}
