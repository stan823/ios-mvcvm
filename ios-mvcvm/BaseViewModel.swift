//
//  BaseViewModel.swift
//  ios-mvcvm
//
//  Created by Stanislaw Brzezinski on 28/01/2017.
//  Copyright © 2017 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
class BaseViewModel<M>{
    
    var model:M? = nil
    
    required init(model:M){
        self.model=model;
    }
}
