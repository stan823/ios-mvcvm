//
//  ViewUpdatable.swift
//  ios-mvcvm
//
//  Created by Stanislaw Brzezinski on 28/01/2017.
//  Copyright © 2017 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
protocol ViewUpdatable {
    func updateView();
}

