//
//  CalculatorController.swift
//  ios-mvcvm
//
//  Created by Stanislaw Brzezinski on 28/01/2017.
//  Copyright © 2017 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
import Darwin
class CalculatorController:BaseController<CalculatorModel,CalculatorViewModel>{
    required init(viewUpdatable: ViewUpdatable, model: CalculatorModel, viewModel: CalculatorViewModel) {
        super.init(viewUpdatable: viewUpdatable, model: model, viewModel: viewModel)
    }
    
    func onSideFieldChanged(value:String){
        
        let side=Double(value)!
        self.model.side=side
        self.model.area=pow(side, 2)
        self.model.diameter=sqrt(2*pow(side,2))
        updateView()
    }
    
    func onAreaFieldChanged(value:String){
        
        let area=Double(value)!
        let side=sqrt(area)
        self.model.area=area
        self.model.side=side
        self.model.diameter=sqrt(2*pow(side,2))
        updateView()
    }
}
