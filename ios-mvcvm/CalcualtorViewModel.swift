//
//  CalcualtorViewModel.swift
//  ios-mvcvm
//
//  Created by Stanislaw Brzezinski on 28/01/2017.
//  Copyright © 2017 Stanislaw Brzezinski. All rights reserved.
//

import Foundation
class CalculatorViewModel:BaseViewModel<CalculatorModel>{
    required init(model:CalculatorModel){
        super.init(model: model)
    }
    
    func getDiameterText()->String{
        let diameter=String(format:"%.2f", self.model!.diameter)
        return "Diameter: \(diameter) cm2"
    }
    
    func getAreaText()->String{
        return String(format:"%.2f", self.model!.area)
    }
    
    func getSideText()->String{
        
        return String(format:"%.2f", self.model!.side)
    }
}
